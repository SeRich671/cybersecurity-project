<?php

use App\Models\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertRolesToRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $roles = [
            [
                'slug' => 'admin',
                'title' => 'Administrator',
                'color' => 'green',
            ],
            [
                'slug' => 'moderator',
                'title' => 'Moderator',
                'color' => '#167cb9',
            ],
            [
                'slug' => 'user',
                'title' => 'Użytkownik',
                'color' => '#000000',
            ],
            [
                'slug' => 'blocked',
                'title' => 'Zbanowany użytkownik',
                'color' => '#ff3333'
            ]
        ];

        Role::insert($roles);
    }
}
