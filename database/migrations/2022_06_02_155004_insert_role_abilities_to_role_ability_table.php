<?php

use App\Models\Ability;
use App\Models\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertRoleAbilitiesToRoleAbilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //@todo Refactor role_abilities
        $admin = Role::where('slug', 'LIKE', 'admin')->first();
        $moderator = Role::where('slug', 'LIKE', 'moderator')->first();
        $user = Role::where('slug', 'LIKE', 'user')->first();
        $blocked = Role::where('slug', 'LIKE', 'blocked')->first();

        $allAbilities = Ability::all()->mapWithKeys(function ($ability) {
            return [
                $ability->id => [
                    'can' => 0,
                ]
            ];
        });

        $adminAbilities = Ability::all()->mapWithKeys(function ($ability) {
            return [
                $ability->id => [
                    'can' => 1
                ]
            ];
        });

        $moderatorAbilities = Ability::where('slug', 'LIKE', 'message.' . '%')
            ->get()
            ->mapWithKeys(function ($ability) {
                return [
                    $ability->id => [
                        'can' => 1
                    ]
                ];
            });

        $userAbilities = Ability::where('slug', 'LIKE', 'message.' . '%')
            ->where('slug', 'NOT LIKE', 'message.edit-others')
            ->get()
            ->mapWithKeys(function ($ability) {
                return [
                    $ability->id => [
                        'can' => 1
                    ]
                ];
            });

        $blockedAbilities = Ability::where('slug', 'LIKE', 'message.index')
            ->get()
            ->mapWithKeys(function ($ability) {
                return [
                    $ability->id => [
                        'can' => 1
                    ]
                ];
            });

        $admin->abilities()->sync($adminAbilities->union($allAbilities));
        $moderator->abilities()->sync($moderatorAbilities->union($allAbilities));
        $user->abilities()->sync($userAbilities->union($allAbilities));
        $blocked->abilities()->sync($blockedAbilities->union($allAbilities));
    }
}
