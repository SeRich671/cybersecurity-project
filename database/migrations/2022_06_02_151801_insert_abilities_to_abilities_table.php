<?php

use App\Models\Ability;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertAbilitiesToAbilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $abilities = [
            [
                'slug' => 'message.index',
                'title' => 'Wiadomości -> Listowanie'
            ],
            [
                'slug' => 'message.create',
                'title' => 'Wiadomości -> Tworzenie',
            ],
            [
                'slug' => 'message.update',
                'title' => 'Wiadomości -> Edycja',
            ],
            [
                'slug' => 'message.update-others',
                'title' => 'Wiadomości -> Edycja wiadomości innych użytkowników',
            ],
            [
                'slug' => 'message.delete',
                'title' => 'Wiadomości -> Usuwanie',
            ],
            [
                'slug' => 'message.delete-others',
                'title' => 'Wiadomości -> Usuwanie wiadomości innych użytkowników',
            ],
            [
                'slug' => 'admin.ability.index',
                'title' => 'Uprawnienia -> Listowanie',
            ],
            [
                'slug' => 'admin.ability.update',
                'title' => 'Uprawnienia -> Edycja',
            ],
            [
                'slug' => 'admin.role.index',
                'title' => 'Grupy uprawnień -> Listowanie',
            ],
            [
                'slug' => 'admin.role.update',
                'title' => 'Grupy uprawnień -> Edycja',
            ],
            [
                'slug' => 'admin.user.index',
                'title' => 'Użytkownicy -> Listowanie',
            ],
            [
                'slug' => 'admin.user.update',
                'title' => 'Użytkownicy -> Edycja',
            ],
            [
                'slug' => 'admin.user.create',
                'title' => 'Użytkownicy -> Tworzenie',
            ],
            [
                'slug' => 'admin.user.delete',
                'title' => 'Użytkownicy -> Usuwanie',
            ],
        ];

        Ability::insert($abilities);
    }
}
