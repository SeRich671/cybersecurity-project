<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class MessageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $user = User::all();

        return [
            'user_id' => $user->random(),
            'title' => $this->faker->sentence(3),
            'content' => $this->faker->sentence(20),
        ];
    }
}
