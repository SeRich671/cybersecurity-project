@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <span class="h2" style="text-shadow: 1px 1px 1px #gray">
                    Edycja uprawnienia
                </span>
            </div>
            <div class="card mt-5 shadow">
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.ability.update', $ability) }}" method="POST">
                        @csrf
                        @method('put')
                        <div class="row">
                            <div class="col-lg-4 offset-lg-4 mt-2">
                                <div class="form-group">
                                    <label for="title">Nazwa</label>
                                    <input type="text" class="form-control" name="title" value="{{ $ability->title }}" required>
                                </div>
                            </div>
                            <div class="col-lg-12 text-center mt-4">
                                <button type="submit" class="btn btn-primary">Zapisz</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
