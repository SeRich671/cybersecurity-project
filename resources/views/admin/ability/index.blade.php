@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <span class="h2" style="text-shadow: 1px 1px 1px #gray">
                    Uprawnienia
                </span>
            </div>
            <div class="card mt-5 shadow">
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Nazwa uprawnienie</th>
                            <th>Alias</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($abilities as $ability)
                            <tr>
                                <td>{{ $ability->title }}</td>
                                <td>{{ $ability->slug }}</td>
                                <td class="text-end">
                                    @can('update', $ability)
                                    <a href="{{ route('admin.ability.edit', $ability) }}" class="btn btn-primary">
                                        Edytuj
                                    </a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
