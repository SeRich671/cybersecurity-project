@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <span class="h2" style="text-shadow: 1px 1px 1px #gray">
                    Grupy uprawnień
                </span>
            </div>
            <div class="card mt-5 shadow">
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Nazwa grupy</th>
                            <th>Alias</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $role)
                            <tr>
                                <td>{{ $role->title }}</td>
                                <td>{{ $role->slug }}</td>
                                <td class="text-end">
                                    @can('update', $role)
                                    <a href="{{ route('admin.role.edit', $role) }}" class="btn btn-primary">
                                        Edytuj
                                    </a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
