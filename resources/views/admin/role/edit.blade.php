@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <span class="h2" style="text-shadow: 1px 1px 1px #gray">
                    Edycja grupy uprawnień
                </span>
            </div>
            <div class="card mt-5 shadow">
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.role.update', $role) }}" method="POST">
                        @csrf
                        @method('put')
                        <div class="row">
                            <div class="col-lg-4 offset-lg-4 mt-2">
                                <div class="form-group">
                                    <label for="title">Nazwa</label>
                                    <input type="text" class="form-control" name="title" value="{{ $role->title }}" required>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <table class="table mt-5">
                                    <thead>
                                        <tr>
                                            <td colspan="2" class="text-center">
                                                <strong>Uprawnienia</strong>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($abilities as $ability)
                                            <tr>
                                                <td>{{ $ability->title }}</td>
                                                <td class="text-end">
                                                    <input type="radio"
                                                           class="btn-check"
                                                           name="can[{{$ability->id}}]"
                                                           id="success-outlined-{{$ability->id}}"
                                                           autocomplete="off"
                                                           value="1"
                                                           @if(isset($roleAbilities[$ability->id]) && $roleAbilities[$ability->id] == 1) checked @endif>
                                                    <label class="btn btn-outline-success" for="success-outlined-{{$ability->id}}">
                                                        Zezwól
                                                    </label>

                                                    <input type="radio"
                                                           class="btn-check"
                                                           name="can[{{$ability->id}}]"
                                                           id="danger-outlined-{{$ability->id}}"
                                                           autocomplete="off"
                                                           value="0"
                                                           @if(!isset($roleAbilities[$ability->id]) || $roleAbilities[$ability->id] == 0) checked @endif>
                                                    <label class="btn btn-outline-danger" for="danger-outlined-{{$ability->id}}">
                                                        Zabroń
                                                    </label>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-12 text-center mt-4">
                                <button type="submit" class="btn btn-primary">Zapisz</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
