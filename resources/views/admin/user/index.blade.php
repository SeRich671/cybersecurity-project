@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <span class="h2" style="text-shadow: 1px 1px 1px #gray">
                    Użytkownicy
                </span>
            </div>
            <div class="card mt-5 shadow">
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Imię</th>
                                <th>Grupa uprawnień</th>
                                <th>Email</th>
                                <th>Data założenia konta</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->role->title }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('d.m.Y, H:i') }}</td>
                                    <td class="text-end">
                                        @can('update', $user)
                                        <a href="{{ route('admin.user.edit', $user) }}" class="btn btn-primary">
                                            Edytuj
                                        </a>
                                        @endcan
                                        @can('delete', $user)
                                        <a class="btn btn-danger" href="{{ route('admin.user.destroy', $user) }}"
                                           onclick="event.preventDefault();
                                                   if(confirm('Czy na pewno chcesz usunąć użytkownika?')){
                                                   document.getElementById('logout-form-{{ $user->id }}').submit();
                                                   }
                                                   ">
                                            {{ __('Usuń') }}
                                        </a>

                                        <form id="logout-form-{{ $user->id }}" action="{{ route('admin.user.destroy', $user) }}" method="POST" class="d-none">
                                            @csrf
                                            @method('delete')
                                        </form>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
