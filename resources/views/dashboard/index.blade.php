@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <span class="h2" style="text-shadow: 1px 1px 1px #gray">
                    Wiadomości
                </span>
            </div>
            <div class="col-lg-8 mt-5">
                @foreach($messages as $message)
                <div class="card mb-3 shadow" style="border-left: 3px solid {{ $message->user->role->color }}">
                    <div class="card-header">
                        <strong>{{ $message->user->name }}</strong> o
                        <strong>{{ \Carbon\Carbon::parse($message->created_at)->format('H:i') }}</strong> napisał:
                    </div>
                    <div class="card-body">
                        <strong class="d-block mb-2">
                            {{ $message->title }}
                        </strong>
                        <span>
                            {{ $message->content }}
                        </span>
                        <small class="text-secondary">
                            {{ $message->created_at != $message->updated_at
                                ? '(ostatnia modyfikacja ' . \Carbon\Carbon::parse($message->updated_at)->format('H:i d.m.Y') . ')' : '' }}
                        </small>
                    </div>
                    @canany(['update', 'delete'], $message)
                    <div class="card-footer text-end">
                        @can('update', $message)
                            <a href="{{ route('message.edit', $message) }}" class="btn btn-primary">
                                Edytuj
                            </a>
                        @endcan
                        @can('delete', $message)
                            <a class="btn btn-danger" href="{{ route('message.destroy', $message) }}"
                               onclick="event.preventDefault();
                                       if(confirm('Czy na pewno chcesz usunąć wiadomość?')){
                                       document.getElementById('logout-form-{{ $message->id }}').submit();
                                       }
                                       ">
                                {{ __('Usuń') }}
                            </a>
                            <form id="logout-form-{{ $message->id }}" action="{{ route('message.destroy', $message) }}" method="POST" class="d-none">
                                @csrf
                                @method('delete')
                            </form>
                        @endcan
                    </div>
                    @endcanany
                </div>
                @endforeach
            </div>
            <div class="col-lg-8 d-flex justify-content-center">
                {{ $messages->links() }}
            </div>
        </div>
    </div>
@endsection
