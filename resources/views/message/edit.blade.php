@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <span class="h2" style="text-shadow: 1px 1px 1px #gray">
                    Edycja wiadomości
                </span>
            </div>
            <div class="card mt-5 shadow">
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="post" action="{{ route('message.update', $message) }}">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="title">Nagłówek</label>
                            <input type="text" class="form-control" name="title" value="{{ $message->title }}">
                        </div>
                        <div class="form-group mt-4">
                            <label for="content">Treść wiadomości</label>
                            <textarea type="text" class="form-control" name="content" rows="10">{{ $message->content }}</textarea>
                        </div>
                        <div class="text-center mt-4">
                            <button type="submit" class="btn btn-primary">Zapisz</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
