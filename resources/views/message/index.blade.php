@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <span class="h2">
                    Moje wiadomoci
                </span>
            </div>
            <div class="col-lg-8">
                @if($messages->isNotEmpty())
                    @foreach($messages as $message)
                        <div class="card mt-5 shadow">
                            <div class="card-header">
                                <strong>{{ $message->title }}</strong>
                                <small class="text-secondary float-end">
                                    {{ \Carbon\Carbon::parse($message->created_at)->format('H:i d.m.Y') }}
                                </small>
                            </div>
                            <div class="card-body">
                                {{ $message->content }}
                            </div>
                            <div class="card-footer text-end">
                                @can('update', $message)
                                <a href="{{ route('message.edit', $message) }}" class="btn btn-primary">
                                    Edytuj
                                </a>
                                @endcan
                                @can('delete', $message)
                                <a class="btn btn-danger" href="{{ route('message.destroy', $message) }}"
                                   onclick="event.preventDefault();
                                   if(confirm('Czy na pewno chcesz usunąć wiadomość?')){
                                       document.getElementById('logout-form-{{ $message->id }}').submit();
                                   }
                                ">
                                    {{ __('Usuń') }}
                                </a>
                                <form id="logout-form-{{ $message->id }}" action="{{ route('message.destroy', $message) }}" method="POST" class="d-none">
                                    @csrf
                                    @method('delete')
                                </form>
                                @endcan
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="col-lg-8 d-flex justify-content-center mt-5">
                {{ $messages->links() }}
            </div>
        </div>
    </div>
@endsection
