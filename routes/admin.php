<?php

use App\Http\Controllers\Admin\AbilityController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

Route::resource('user', UserController::class, [
    'only' => ['index', 'edit', 'update', 'destroy']
]);

Route::resource('ability', AbilityController::class, [
    'only' => ['index', 'edit', 'update']
]);

Route::resource('role', RoleController::class, [
    'only' => ['index', 'edit', 'update']
]);
