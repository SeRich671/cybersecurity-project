<?php

namespace App\Policies;

use App\Models\Ability;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AbilityPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        return $user->policies['admin.ability.index'];
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Ability  $ability
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Ability $ability)
    {
        return $user->policies['admin.ability.update'];
    }
}
