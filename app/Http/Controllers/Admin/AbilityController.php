<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Ability;
use App\Models\Message;
use Illuminate\Http\Request;

class AbilityController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Ability::class, 'ability');
    }

    public function index()
    {
        $abilities = Ability::all();

        return view('admin.ability.index', [
            'abilities' => $abilities
        ]);
    }

    public function edit(Ability $ability)
    {
        return view('admin.ability.edit', [
            'ability' => $ability,
        ]);
    }

    public function update(Request $request, Ability $ability)
    {
        $ability->update([
            'title' => $request->input('title'),
        ]);

        return redirect()->route('admin.ability.index');
    }
}
