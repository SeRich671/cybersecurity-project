<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\UpdateRequest;
use App\Models\Ability;
use App\Models\Message;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    const FILTER_ABILITY_VALUE = 2;

    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    public function index()
    {
        $users = User::all();

        return view('admin.user.index', [
            'users' => $users,
        ]);
    }

    public function edit(User $user)
    {
        $roles = Role::all();
        $abilities = Ability::all();
        $userAbilities = $user->abilities()
            ->withPivot(['can'])
            ->get()
            ->mapWithKeys(function ($userAbility) {
                return [
                    $userAbility->id => $userAbility->pivot->can,
                ];
            });

        return view('admin.user.edit', [
            'user' => $user,
            'roles' => $roles,
            'abilities' => $abilities,
            'userAbilities' => $userAbilities,
        ]);
    }

    public function update(UpdateRequest $request, User $user): RedirectResponse
    {
        $data = $request->validated();
        $abilities = collect($data['can'])->reject(function ($ability) {
            return $ability == self::FILTER_ABILITY_VALUE;
        })->mapWithKeys(function ($ability, $id) {
            return [
                $id => [
                    'can' => $ability,
                ]
            ];
        });

        $user->update($data);
        $user->abilities()->sync($abilities);

        return redirect()->route('admin.user.index')->with([
            'success' => 'Pomyślnie zaktualizowano użytkownika'
        ]);
    }

    public function destroy(User $user): RedirectResponse
    {
        $user->delete();

        return redirect()->route('admin.user.index')->with([
            'success' => 'Pomyślnie zaktualizowano użytkownika'
        ]);
    }
}
