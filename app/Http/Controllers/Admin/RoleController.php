<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Role\UpdateRequest;
use App\Models\Ability;
use App\Models\Message;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Role::class, 'role');
    }

    public function index()
    {
        $roles = Role::all();

        return view('admin.role.index', [
            'roles' => $roles,
        ]);
    }

    public function edit(Role $role)
    {
        $abilities = Ability::all();
        $roleAbilities = $role->abilities()
            ->withPivot(['can'])
            ->get()
            ->mapWithKeys(function ($roleAbility) {
                return [
                    $roleAbility->id => $roleAbility->pivot->can,
                ];
            });

        return view('admin.role.edit', [
            'abilities' => $abilities,
            'role' => $role,
            'roleAbilities' => $roleAbilities->toArray(),
        ]);
    }

    public function update(UpdateRequest $request, Role $role)
    {
        $data = $request->validated();

        $role->update([
            'title' => $data['title'],
        ]);

        $abilities = collect($data['can'])->mapWithKeys(function ($value, $key) {
            return [
                $key => [
                    'can' => $value
                ]
            ];
        });

        $role->abilities()->sync($abilities);

        return redirect()->route('admin.role.index');
    }

    public function destroy(Role $role)
    {
        $role->delete();

        return redirect()->route('admin.role.index');
    }
}
