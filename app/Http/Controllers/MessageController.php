<?php

namespace App\Http\Controllers;

use App\Http\Requests\Message\StoreRequest;
use App\Http\Requests\Message\UpdateRequest;
use App\Models\Message;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Message::class, 'message');
    }

    public function index()
    {
        $messages = Auth::user()->messages
            ? Auth::user()->messages()->orderByDesc('created_at')->paginate(10)
            : collect();

        return view('message.index', [
            'messages' => $messages,
        ]);
    }

    public function create()
    {
        return view('message.create');
    }

    public function store(StoreRequest $request): RedirectResponse
    {
        Auth::user()->messages()->create($request->validated());

        return redirect()->route('dashboard')->with([
            'success' => "Pomyślnie utworzono wiadomość"
        ]);
    }

    public function edit(Message $message)
    {
        return view('message.edit', [
            'message' => $message,
        ]);
    }

    public function update(UpdateRequest $request, Message $message): RedirectResponse
    {
        $message->update($request->validated());

        return redirect()->route('dashboard')->with([
            'success' => "Pomyślnie zaktualizowano wiadomość"
        ]);
    }

    public function destroy(Message $message): RedirectResponse
    {
        $message->delete();

        return redirect()->route('message.index')->with([
            'success' => "Pomyślnie usunięto wiadomość"
        ]);
    }
}
